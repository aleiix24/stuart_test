import React, { Component } from "react";

import Map from "./components/Map";
import { ToastProvider } from "./components/Toast";

class App extends Component {
  render() {
    return (
      <div className="App">
        <ToastProvider>
          <Map />
        </ToastProvider>
      </div>
    );
  }
}

export default App;

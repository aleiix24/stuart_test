function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }
  throw new Error(response.status);
}

export function geoCode({ address }) {
  return fetch(`${process.env.REACT_APP_API_ENDPOINT}/geocode`, {
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    method: "POST",
    body: JSON.stringify({ address })
  })
    .then(checkStatus)
    .then(response => response.json());
}

export function submitJob({ pickup, dropoff }) {
  return fetch(`${process.env.REACT_APP_API_ENDPOINT}/jobs`, {
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    method: "POST",
    body: JSON.stringify({ pickup, dropoff })
  })
    .then(checkStatus)
    .then(response => response.json());
}

import React from "react";
import styled from "styled-components";

const StyledInput = styled.input`
  border-radius: 4px;
  color: #252525;
  background-color: #f0f3f7;
  height: 32px;
  border: 0;
  width: 100%;
  display: block;
  padding: 0 5px;
  box-sizing: border-box;
  font-size: 12px;

  &::placeholder {
    color: #8596a6;
  }
`;
function Input(props) {
  return <StyledInput type="text" {...props} />;
}

export default Input;

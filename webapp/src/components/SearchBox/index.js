import React from "react";
import styled from "styled-components";

import { useToast } from "../Toast";
import Input from "./Input";
import Button from "./Button";

import { geoCode, submitJob } from "./requests";
import { PICK_UP, DROP_OFF } from "./constants";

import dropOffBadgeBlank from "./assets/dropOffBadgeBlank.svg";
import dropOffBadgeError from "./assets/dropOffBadgeError.svg";
import dropOffBadgePresent from "./assets/dropOffBadgePresent.svg";
import pickUpBadgeError from "./assets/pickUpBadgeError.svg";
import pickUpBadgeBlank from "./assets/pickUpBadgeBlank.svg";
import pickUpBadgePresent from "./assets/pickUpBadgePresent.svg";
import pickUpMarker from "./assets/pickUpMarker.svg";
import dropOffMarker from "./assets/dropOffMarker.svg";

const Wrapper = styled.div`
  box-shadow: 0 1px 2px 0 rgba(0, 0, 0, 0.1), 0 1px 8px 0 rgba(0, 0, 0, 0.1);
  position: absolute;
  top: 0;
  left: 0;
  margin: 32px;
  display: flex;
  flex-direction: column;
  width: 300px;
  z-index: 10;
  background-color: #fff;
  border-radius: 8px;
`;
const AddressWrapper = styled.div`
  margin: 16px 16px 0;
  display: flex;
  flex-direction: row;
`;
const SubmitWrapper = styled.div`
  margin: 16px;
  padding-left: 40px;
`;
const LogoWrapper = styled.span`
  margin-right: 8px;
`;

function renderBadgeIcon(valid, actionType) {
  if (actionType === PICK_UP) {
    if (valid === undefined) {
      return pickUpBadgeBlank;
    } else if (valid) {
      return pickUpBadgePresent;
    }
    return pickUpBadgeError;
  } else {
    if (valid === undefined) {
      return dropOffBadgeBlank;
    } else if (valid) {
      return dropOffBadgePresent;
    }
    return dropOffBadgeError;
  }
}

function SearchBox({ setMarkers }) {
  const [loading, setLoading] = React.useState(false);
  const [pickUp, setPickUp] = React.useState(undefined);
  const [dropOff, setDropOff] = React.useState(undefined);
  const { setToast } = useToast();

  const submitDisabled = loading || !dropOff || !pickUp;

  function geolocalize(address, action) {
    geoCode({ address })
      .then(({ latitude, longitude }) => {
        if (action === PICK_UP) {
          setPickUp({ address, latitude, longitude });
        } else if (action === DROP_OFF) {
          setDropOff({ address, latitude, longitude });
        }
        setMarkers([
          {
            latitude: latitude,
            longitude: longitude,
            icon: action === PICK_UP ? pickUpMarker : dropOffMarker
          }
        ]);
      })
      .catch(err => {
        if (action === PICK_UP) {
          setPickUp(null);
        } else if (action === DROP_OFF) {
          setDropOff(null);
        }
      });
  }

  function submit() {
    setLoading(true);
    submitJob({ pickup: pickUp.address, dropoff: dropOff.address }).then(
      response => {
        setLoading(false);
        setToast("Job has been created successfully!");
      }
    );
  }

  return (
    <Wrapper>
      <AddressWrapper>
        <LogoWrapper>
          <img src={renderBadgeIcon(pickUp, PICK_UP)} alt="Pick up address" />
        </LogoWrapper>
        <Input
          placeholder="Pick up address"
          onBlur={e => geolocalize(e.target.value, PICK_UP)}
          onKeyUp={e => geolocalize(e.target.value, PICK_UP)}
        />
      </AddressWrapper>
      <AddressWrapper>
        <LogoWrapper>
          <img
            src={renderBadgeIcon(dropOff, DROP_OFF)}
            alt="Drop off address"
          />
        </LogoWrapper>
        <Input
          placeholder="Drop off address"
          onBlur={e => geolocalize(e.target.value, DROP_OFF)}
          onKeyUp={e => geolocalize(e.target.value, DROP_OFF)}
        />
      </AddressWrapper>
      <SubmitWrapper>
        <Button
          onClick={() => !submitDisabled && submit()}
          disabled={submitDisabled}
        >
          {loading ? "Creating" : "Create job"}
        </Button>
      </SubmitWrapper>
    </Wrapper>
  );
}

export default SearchBox;

import React from "react";
import styled from "styled-components";

const StyledButton = styled.button`
  border-radius: 4px;
  height: 40px;
  box-shadow: 0 1px 2px 0 rgba(16, 162, 234, 0.3);
  color: #fff;
  font-weight: 500;
  font-size: 12px;
  cursor: pointer;
  background-image: linear-gradient(#10a2ea, #0f99e8);
  border: 0;
  width: 100%;

  &[disabled] {
    opacity: 0.5;
    cursor: inherit;
  }
`;
function Button({ children, onClick, disabled }) {
  return (
    <StyledButton type="submit" onClick={e => onClick(e)} disabled={disabled}>
      {children}
    </StyledButton>
  );
}

export default Button;

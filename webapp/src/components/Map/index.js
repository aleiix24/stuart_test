import React from "react";
import SearchBox from "../SearchBox/index";

function loadJS(src) {
  var ref = window.document.getElementsByTagName("script")[0];
  var script = window.document.createElement("script");
  script.src = src;
  script.async = true;
  ref.parentNode.insertBefore(script, ref);
}

function Map() {
  const mapRef = React.useRef(null);
  React.useEffect(() => {
    window.initMap = initMap;
    loadJS(
      `https://maps.googleapis.com/maps/api/js?key=${
        process.env.REACT_APP_GOOGLE_MAP_KEY
      }&callback=initMap`
    );
  }, []);

  let map = null;
  function initMap() {
    map = new window.google.maps.Map(mapRef.current, {
      zoom: 14,
      center: new window.google.maps.LatLng(48.864716, 2.349014), // location paris
      mapTypeId: "roadmap"
    });
  }

  function setMarker({ lat, lng, icon }) {
    new window.google.maps.Marker({
      position: { lat, lng },
      map: map,
      icon
    });
  }

  return (
    <div>
      <SearchBox
        setMarkers={markers =>
          markers.forEach(({ latitude, longitude, icon }) => {
            setMarker({
              lat: latitude,
              lng: longitude,
              icon
            });
          })
        }
      />
      <div
        ref={mapRef}
        style={{ width: "100%", height: "100%", position: "absolute" }}
      />
    </div>
  );
}

export default Map;

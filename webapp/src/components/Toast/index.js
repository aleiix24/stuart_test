import React from "react";

const Ctx = React.createContext();

const ToastWrapper = props => (
  <div
    style={{
      position: "fixed",
      right: 0,
      top: 0
    }}
    {...props}
  />
);
const Toast = ({ text, onClick, remove }) => {
  React.useEffect(() => {
    let timer = setTimeout(() => remove(), 5000);

    return () => {
      clearTimeout(timer);
    };
  }, []);

  return (
    <div
      style={{
        background: "rgba(51,51,51,0.9)",
        height: "40px",
        fontSize: "12px",
        margin: "32px",
        borderRadius: "8px",
        boxShadow: "0 1px 2px 0 rgba(0,0,0,0.1), 0 1px 8px 9 rgba(0,0,0,0.1)",
        padding: "0 10px",
        color: "#fff",
        display: "flex",
        flexDirection: "row",
        alignItems: "center"
      }}
      onClick={() => onClick()}
    >
      {text}
    </div>
  );
};

export function ToastProvider({ children }) {
  const [toast, setToast] = React.useState(null);

  const set = text => {
    setToast({ text });
  };
  const remove = () => {
    setToast(null);
  };

  return (
    <Ctx.Provider value={{ setToast: set }}>
      {children}
      {toast && (
        <ToastWrapper>
          <Toast
            text={toast.text}
            onClick={() => remove()}
            remove={() => remove()}
          />
        </ToastWrapper>
      )}
    </Ctx.Provider>
  );
}

export const useToast = () => React.useContext(Ctx);
